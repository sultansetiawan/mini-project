// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

double defaultMargin = 24.0;
double defaultRadius = 17;

Color kPrimaryColor = Color(0xff5C40CC);
Color kWhiteColor = Color(0xffFFFFFF);
Color kBlackColor = Color(0xff1F1449);
Color kGreyColor = Color(0xff9698A9);
Color kInactiveColor = Color(0xffDBD7EC);
Color kRedColor = Color(0xff0EC3AE);
Color kGreenColor = Color(0xffEB70A5);
Color kBackgroundColor = Color(0xffFAFAFA);
Color kTransparenColor = Colors.transparent;

TextStyle purpleTextStyle = GoogleFonts.poppins(color: kPrimaryColor);
TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlackColor);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhiteColor);
TextStyle redTextStyle = GoogleFonts.poppins(color: kRedColor);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGreyColor);
TextStyle greenTextStyle = GoogleFonts.poppins(color: kGreenColor);

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;
