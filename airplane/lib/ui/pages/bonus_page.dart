import 'package:airplane/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import '../../shared/theme.dart';

class BonusPage extends StatelessWidget {
  const BonusPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget card() {
      return Container(
        width: 300,
        height: 211,
        padding: EdgeInsets.all(defaultMargin),
        decoration: BoxDecoration(
            image: const DecorationImage(
                image: AssetImage('assets/image_card.png')),
            boxShadow: [
              BoxShadow(
                  color: kPrimaryColor.withOpacity(0.5),
                  blurRadius: 50,
                  offset: const Offset(0, 10))
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Name',
                        style: whiteTextStyle.copyWith(fontWeight: regular),
                      ),
                      Text(
                        'Sultan Setiawan',
                        style: whiteTextStyle.copyWith(
                            fontSize: 20, fontWeight: medium),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 24,
                  height: 24,
                  margin: const EdgeInsets.only(right: 5),
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/icon_plane.png'))),
                ),
                Text(
                  'pay',
                  style:
                      whiteTextStyle.copyWith(fontSize: 20, fontWeight: medium),
                )
              ],
            ),
            const SizedBox(
              height: 41,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'balance',
                  style:
                      whiteTextStyle.copyWith(fontWeight: light, fontSize: 14),
                ),
                Text(
                  'IDR 189.000.000',
                  style:
                      whiteTextStyle.copyWith(fontWeight: medium, fontSize: 24),
                )
              ],
            )
          ],
        ),
      );
    }

    Widget tittle() {
      return Container(
        margin: const EdgeInsets.only(top: 65),
        child: Column(
          children: [
            Text(
              'Big Bonus 🎉',
              style:
                  blackTextStyle.copyWith(fontWeight: semiBold, fontSize: 32),
            ),
          ],
        ),
      );
    }

    Widget description() {
      return Container(
        margin: const EdgeInsets.only(top: 20),
        child: Column(children: [
          Text(
            'We give you early credit so that\nyou can buy a flight ticket',
            style: greyTextStyle.copyWith(fontWeight: light, fontSize: 16),
            textAlign: TextAlign.center,
          ),
        ]),
      );
    }

    Widget startFly() {
      return Container(
        margin: const EdgeInsets.only(top: 65),
        child: Column(
          children: [
            CustomButton(
                width: 220,
                title: 'Start Flying Now',
                onPressed: () {
                  Navigator.pushNamed(context, '/main');
                })
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [card(), tittle(), description(), startFly()],
        ),
      ),
    );
  }
}
