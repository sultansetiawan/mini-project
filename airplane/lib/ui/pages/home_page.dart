import 'package:airplane/ui/widgets/new_destination.dart';
import 'package:airplane/ui/widgets/popular_destination.dart';
import 'package:flutter/material.dart';
import '../../shared/theme.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: const EdgeInsets.only(left: 24, right: 24, top: 30),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Howdy,\nSultan Setiawan',
                    style: blackTextStyle.copyWith(
                        fontWeight: semiBold, fontSize: 24),
                    overflow: TextOverflow.ellipsis,
                  ),
                  Text(
                    'Where to fly today',
                    style: greenTextStyle.copyWith(
                        fontWeight: light, fontSize: 16),
                  )
                ],
              ),
            ),
            Container(
              width: 60,
              height: 60,
              decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  image:
                      DecorationImage(image: AssetImage('assets/profile.jpg'))),
            )
          ],
        ),
      );
    }

    Widget popularDestination() {
      return Container(
        margin: const EdgeInsets.only(left: 24, right: 24, top: 30),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: const [
              PopularDestination(
                  place: 'Lake Ciliwung',
                  city: 'Tangerang',
                  imageUrl: 'assets/image_destination1.png'),
              PopularDestination(
                  place: 'White House',
                  city: 'Spain',
                  imageUrl: 'assets/image_destination2.png'),
              PopularDestination(
                  place: 'Hill Heyo',
                  city: 'Monaco ',
                  imageUrl: 'assets/image_destination3.png'),
              PopularDestination(
                  place: 'Menara Japan',
                  city: 'japan',
                  imageUrl: 'assets/image_destination4.png'),
              PopularDestination(
                  place: 'Payung Teduh',
                  city: 'Singapore',
                  imageUrl: 'assets/image_destination5.png')
            ],
          ),
        ),
      );
    }

    Widget newDestination() {
      return Container(
        margin:
            EdgeInsets.only(top: 30, left: defaultMargin, right: defaultMargin),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'New This Year',
              style:
                  blackTextStyle.copyWith(fontWeight: semiBold, fontSize: 18),
            ),
            const NewDestination(
                place: 'Danau Beratan',
                city: 'Singaraja',
                imageURL: 'assets/image_destination6.png'),
            const NewDestination(
                place: 'Sydney Opera',
                city: 'Australia',
                imageURL: 'assets/image_destination7.png'),
            const NewDestination(
                place: 'Roma',
                city: 'Italy',
                imageURL: 'assets/image_destination8.png'),
            const NewDestination(
                place: 'Payung Teduh',
                city: 'Singapore',
                imageURL: 'assets/image_destination9.png'),
            const NewDestination(
                place: 'Hill Hey',
                city: 'Monaco',
                imageURL: 'assets/image_destination10.png'),
          ],
        ),
      );
    }

    return ListView(
      children: [
        header(),
        popularDestination(),
        newDestination(),
      ],
    );
  }
}
