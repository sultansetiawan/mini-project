// ignore_for_file: prefer_const_constructors

import 'package:airplane/ui/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import '../../shared/theme.dart';

class StartedPage extends StatelessWidget {
  const StartedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  'assets/image_get_started.png',
                ),
              ),
            ),
          ),
          Center(
            child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
              Text(
                'Fly Like a Birds!',
                style: whiteTextStyle.copyWith(
                  fontWeight: semiBold,
                  fontSize: 32,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'Explore new worl with us and let\nYourself get an amazing experiences',
                style: whiteTextStyle.copyWith(fontWeight: light, fontSize: 16),
                textAlign: TextAlign.center,
              ),
              CustomButton(
                  title: 'Get Started',
                  width: 220,
                  margin: EdgeInsets.only(top: 50, bottom: 80),
                  onPressed: () {
                    Navigator.pushNamed(context, '/signup');
                  })
            ]),
          ),
        ],
      ),
    );
  }
}
